package com.locationrecommender.dbconnector;

import com.locationrecommender.config.Config;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.*;


/**
 * Created by Pablo Barrenechea on 12/10/2015.
 */
public class PostgresConnector  {

    private static final Logger logger = LogManager.getLogger(PostgresConnector.class);
    private Connection conn = null;
    private Statement stmt = null;
    private ResultSet lastResults;

    public PostgresConnector(){ }

    public void connect() {
        try {
            conn = DriverManager.getConnection("jdbc:postgresql://" + Config.getInstance().getProperty("postgres.database_host")
                            + ":" + Config.getInstance().getProperty("postgres.database_port") + "/"
                            + Config.getInstance().getProperty("postgres.database_name"),
                    Config.getInstance().getProperty("postgres.database_user"),
                    Config.getInstance().getProperty("postgres.database_password"));
            logger.debug("Succesfully connected to Postgres");
        } catch (SQLException e) {
            logger.debug("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }
    }

    public void executeQuery(String query) throws SQLException {
        this.stmt = conn.createStatement();
        this.lastResults = this.stmt.executeQuery(query);
    }

    public void closeResultSet() throws SQLException {
        this.stmt.close();
        this.lastResults.close();
    }

    public ResultSet getLastResults(){
        return lastResults;
    }
}
