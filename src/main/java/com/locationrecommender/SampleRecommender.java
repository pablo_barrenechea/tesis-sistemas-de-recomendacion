package com.locationrecommender;

import com.locationrecommender.dataset.Dataset;
import com.locationrecommender.dataset.PostgresDataset;
import com.locationrecommender.dataset.entities.Checkin;
import com.locationrecommender.dataset.entities.User;
import com.locationrecommender.dataset.entities.Venue;
import com.locationrecommender.filter.QueryFilter;
import com.locationrecommender.filter.sql.AndFilter;
import com.locationrecommender.filter.sql.CoordanatesFilter;
import com.locationrecommender.filter.sql.SimpleFieldFilter;
import com.locationrecommender.geo.Coordinates;
import com.locationrecommender.geo.Point;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.mahout.cf.taste.common.TasteException;

import org.apache.mahout.cf.taste.eval.IRStatistics;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderEvaluator;
import org.apache.mahout.cf.taste.eval.RecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.eval.AverageAbsoluteDifferenceRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.eval.GenericRecommenderIRStatsEvaluator;

import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderEvaluator;
import org.apache.mahout.cf.taste.eval.RecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.eval.AverageAbsoluteDifferenceRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.eval.GenericRecommenderIRStatsEvaluator;

import org.apache.mahout.cf.taste.impl.eval.RMSRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;

import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericBooleanPrefUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.LogLikelihoodSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.UncenteredCosineSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.apache.mahout.common.RandomUtils;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class SampleRecommender {

	private static final Logger logger = LogManager.getLogger(SampleRecommender.class);

	public static void main(String[] args) throws IOException, TasteException, SQLException {
		
		BasicConfigurator.configure();

		DataModel model = new FileDataModel(new File("C:/ratings.csv"));
		
		RandomUtils.useTestSeed();
		
		 UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
		 //UserSimilarity similarity = new UncenteredCosineSimilarity(model);
		 //UserNeighborhood neighborhood = new NearestNUserNeighborhood(500, similarity, model);
		  
		/*System.out.println(neighborhood.getUserNeighborhood(45).length);
		  UserBasedRecommender recommender = new
		  GenericUserBasedRecommender(model, neighborhood, similarity);
		  List<RecommendedItem> nuevo = recommender.recommend(45, 10); for
		  (RecommendedItem recommendation : nuevo) {
		  System.out.println(recommendation); } System.out.println("END");
		  System.exit(0);*/
		 
		RecommenderBuilder builder = new MyRecommenderBuilder();

		RecommenderIRStatsEvaluator evaluator = new GenericRecommenderIRStatsEvaluator();
		IRStatistics stats = evaluator.evaluate(builder, null, model, null, 10, 0.9, 0.8);
		System.out.format("The recommender precision is %f%n", stats.getPrecision());
		System.out.format("The recommender recall is %f%n", stats.getRecall());
		System.out.format("The recommender F1 is %f%n", stats.getF1Measure());
		/*
		 * RecommenderEvaluator evaluator = new
		 * AverageAbsoluteDifferenceRecommenderEvaluator(); RecommenderBuilder
		 * builder = new MyRecommenderBuilder(); double result =
		 * evaluator.evaluate(builder, null, model, 0.8, 1.0); logger.debug(
		 * "Recommendation metrics: " + result); logger.debug("Presicion: " );
		 * 
		 * RecommenderEvaluator rmse = new RMSRecommenderEvaluator(); result =
		 * rmse.evaluate(builder, null, model, 0.8, 1.0); logger.debug(
		 * "RMSE metrics: " + result);
		 */

		/*
		 * RecommenderEvaluator evaluator = new
		 * AverageAbsoluteDifferenceRecommenderEvaluator(); RecommenderBuilder
		 * builder = new MyRecommenderBuilder(); double result =
		 * evaluator.evaluate(builder, null, model, 0.9, 1.0);
		 * System.out.println(result);
		 */

	}

}
