package com.locationrecommender.loader;

import java.io.File;

/**
 * Created by Pablo Barrenechea on 10/19/2015.
 */
public class FileLoader {

    private ClassLoader classLoader;

    public FileLoader(){
        classLoader = getClass().getClassLoader();
    }

    public File loadFileFromResources(String fileName){
        return new File(classLoader.getResource(fileName).getFile());
    }
}
